/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "fs.h"
#include <string.h>
#include <unistd.h>

int does_exefile_exists(const char* path)
{
  // TODO: Fill it!
  char buff[1024] = {0, };

  if (strstr(path, "zombie") != NULL)
    return 1;

  else if(readlink(path, buff, 1024)) return 1;
  
  else
    return 0;
}
