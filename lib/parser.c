/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "parser.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define HEIGHT 100

void parse_command(const char* input, int* argc, char*** argv)
{
	// TODO: Fill it!
	(*argc) = -1;
	(*argv) = (char**)malloc(sizeof(char*)*HEIGHT);
	for (int i = 0; i < HEIGHT; i++)
		(*argv)[i] = NULL;

	char* tmp = (char*)malloc(strlen(input));
	strcpy(tmp, input);
	char* command = strtok(tmp, " \n");

	while (command != NULL){
		(*argv)[++(*argc)] = (char*)malloc(sizeof(char)*(strlen(command) + 1));
		strncpy((*argv)[(*argc)], command, strlen(command));
		(*argv)[(*argc)][strlen(command)] = '\0';

		if (command[strlen(command) + 1] == '\"'){
			command = strtok(NULL, "\"");
			continue;
		}

		command = strtok(NULL, " \n");
	}

	(*argc) += 1;
	free(tmp);
}

