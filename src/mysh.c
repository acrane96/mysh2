/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include "parser.h"
#include "utils.h"
#include "fs.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <wait.h>

#define PIPEFILE "./pipefile"
#define ZOMBIEFILE "./zombiefile"

void zombie();

typedef struct{
  unsigned int __sigbits[4];
}sigset_t;

int main()
{
  //signal
  signal(SIGINT, SIG_IGN);
  signal(SIGTSTP, SIG_IGN);

  //zombie

  char command_buffer[4096] = { 0, };

  while (fgets(command_buffer, 4096, stdin) != NULL) {
    if(strlen(command_buffer) == 1)
      continue;
      
    int fd;
    int status;
    int argc = -1;
    char** argv = NULL;
    pid_t pid;

    parse_command(command_buffer, &argc, &argv);

    assert(argv != NULL);
    if (strcmp(argv[0], "exit") == 0) {
      FREE_2D_ARRAY(argc, argv);
      break;
    }

    struct command_entry* comm_entry = fetch_command(argv[0]);

    if(strchr(command_buffer, '|') != NULL){    //파이프 처리
      do_pipe(argc, argv);
    }

    else if (comm_entry != NULL) {
      int ret = comm_entry->command_func(argc, argv);
      if (ret != 0) {
        comm_entry->err(ret);
      }
    } 
    
    else if (does_exefile_exists(argv[0])) {
    // TODO: Execute the program of argv[0].
      if(strcmp(argv[0], "fg") == 0){
        waitpid(0, &status, 0);
        continue;
      }
      int background = 0;
      if(strstr(argv[argc-1], "&")){
        background = 1;
        argv[argc - 1] = '\0';
      }

      pid = fork();
      if(pid == -1){
        printf("fork 실패\n");
        continue;
      }

      else if(pid == 0){
        if(!strcmp(argv[0], "zombie"))
           zombie();
        execvp(argv[0], argv);
      }
      else{
        if(background)
          waitpid(-1, &status, WNOHANG);

        else if(!strcmp(argv[0], "zombie")){
          waitpid(-1, &status, WNOHANG);
        }

        else wait(NULL);
      }
    }
    else {
      assert(comm_entry == NULL);
      fprintf(stderr, "%s: command not found.\n", argv[0]);
    }

    FREE_2D_ARRAY(argc, argv);
  }
  
  char buff[1024];
  pid_t zombiepid = fork();
//  FILE* readn = fopen(PIPEFILE, "w+");
//  FILE* file = fopen(ZOMBIEFILE, "w+");
  int fd, newfd;
  fd = open(PIPEFILE, O_RDWR | O_CREAT, 0644);

  if(zombiepid == 0){
    pid_t pid2 = fork();

    if(pid2 == 0){
      dup2(fd, 1);
      execlp("ps", "ps", 0);
    }

    else {
      wait(NULL);
      newfd = open(ZOMBIEFILE, O_RDWR | O_CREAT, 0644);
      dup2(newfd, 1);
      execlp("grep", "grep", "defunct", PIPEFILE, "-a", 0);
    }
    exit(1);
  }

  else {
    wait(NULL);
    newfd = open(ZOMBIEFILE, O_RDWR | O_CREAT, 0644);
    read(newfd, buff, 1024);

    if((int)strlen(buff) != 0){  
      printf("\nzombie process alert!\n");
    }
    close(fd);
    close(newfd);
    unlink(PIPEFILE);
    unlink(ZOMBIEFILE);
    exit(1);
  }
  return 0;
}

//zombie process
void zombie()
{
  pid_t pid;
  int status;

  pid = fork();
  if(pid == -1){
    printf("zombie fork 실패\n");
    exit(1);
  }

  else if(pid == 0)
    exit(0);

  sleep(1000);
  
  pid = wait(&status);
  if(WIFEXITED(status))
  fprintf(stderr, "\n\t[%d]\tProcess %d exited with status %d.\n", (int)getpid(), pid, WEXITSTATUS(status));

  return;
}
