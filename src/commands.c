/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "utils.h"       //
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>   //
#include <sys/types.h>   //
#include <fcntl.h>       //
#include <stdlib.h>      //
#include <signal.h>
#include <sys/un.h>     //
#include <pthread.h>
#include <wait.h>

#define _POSIX_SOURCE

#define FILE_SERVER "/tmp/pipe_server"
#define PIPEFILE "./pipefile"
#define BUFF_SIZE 1024

#define MAX_COMMAND 100

static struct command_entry commands[] =
{
  {
    "cd",
    do_cd,
    err_cd
  },
  {
    "kill",
    do_kill,
    err_kill
  }
};

struct command_entry* fetch_command(const char* command_name)
{
  // TODO: Fill it.
  for(int i = 0; i < 2; i++){
 	if (strcmp(command_name, commands[i].command_name) == 0)
		return &commands[i];
  }
  return NULL;
}

int do_cd(int argc, char** argv)
{
  // TODO: Fill it.
  if(chdir(argv[1]) == -1){
    switch(errno){
      case ENOENT:
        return 1;
      case ENOTDIR:
        return 2;
      default:
        printf("unknown error\n"); break;
    }
  }

  return 0;
}

void err_cd(int err_code)
{
  // TODO: Fill it.
  switch(err_code){
    case 1:
      fprintf(stderr, "cd: no such file or directory\n"); break;
    case 2:
      fprintf(stderr, "cd: not a directory\n"); break;
    default:
      printf("wrong err_code\n"); break;
  }
}

  struct MultipleArg{
    char **command1;
    char **command2;
  };

void do_pipe(int argc, char** argv)
{
  int i, j, status;
  pthread_t pipethread[2];
  char **command1;
  char **command2;
  
  command1 = (char**)malloc(sizeof(char*)*10);
  command2 = (char**)malloc(sizeof(char*)*10);
  
  for(int k = 0; k < 10; k++){
    command1[k] = NULL;
    command2[k] = NULL;
  }
  
  for(i = 0; argv[i] != NULL && i < 10 && strchr(argv[i], '|') == NULL; i++){
    command1[i] = (char*)malloc(sizeof(char)*(strlen(argv[i])+1));
    strncpy(command1[i], argv[i], strlen(argv[i]));
	  command1[i][strlen(argv[i])] = '\0';
  }
  
  for(j = 0; argv[++i] != NULL && j < 10; j++){
    command2[j] = (char*)malloc(sizeof(char)*(strlen(argv[i])+1));
    strncpy(command2[j], argv[i], strlen(argv[i]));
	  command2[j][strlen(argv[i])] = '\0';
  }
  
  pthread_create(&pipethread[0], NULL, start_client, (void*)command2);
  pthread_create(&pipethread[1], NULL, start_server, (void*)command1);

  for(int l = 0; l < 2; l++)
    while(pthread_kill(pipethread[l], 0) == 0){sleep(1);}
  for(int m = 0; m < 2; m++){
    pthread_join(pipethread[m], NULL);
  }

  return;
}

void *start_server(void* command1)
{
  int server_socket;
  char buff[BUFF_SIZE] = {0, };
  
  if(access(FILE_SERVER, F_OK) == 0)
    unlink(FILE_SERVER);

  server_socket = socket(PF_FILE, SOCK_STREAM, 0);

  struct sockaddr_un server_addr;
  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sun_family = AF_UNIX;
  strcpy(server_addr.sun_path, FILE_SERVER);

  if(bind(server_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1)
  {
      printf("bind()실행 에러\n");
      exit(1);
  }
  
  if(listen(server_socket, 5) == -1)
  {
    printf("대기상태 모드 설정 실패\n");
    exit(1);
  }
  
  struct sockaddr_un client_addr;
  int client_socket;
  int client_addr_size;
  int fd;

  client_addr_size = sizeof(client_addr);
  client_socket = accept(server_socket, (struct sockaddr*)&client_addr, &client_addr_size);
  
  if(client_socket == -1)
  {
    printf("클라이언트 연결 수락 실패\n");
    exit(1);
  }
  
  int pid = fork();
  if(pid == -1){
    printf("fork 실패\n");
    exit(1);
  }
  else if(pid == 0){
    dup2(client_socket, 1);
    execvp(((char**)command1)[0], (char**)command1);
  }
  else 
    wait(NULL);

  close(client_socket);
  FREE_2D_ARRAY(10, (char**)command1);
}

void *start_client(void* command2)
{
  int fd, client_socket;
  char buff[1024] = {0, };
  client_socket = socket(PF_FILE, SOCK_STREAM, 0);
  
  struct sockaddr_un server_addr;
  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sun_family = AF_UNIX;
  strcpy(server_addr.sun_path, FILE_SERVER);

  if(connect(client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)))
  {
    printf("접속 실패\n");
    exit(1);
  }
  
  int pid = fork();
  if(pid == -1){
    printf("fork 실패\n");
    exit(1);
  }
  else if(pid == 0){
    read(client_socket, buff, BUFF_SIZE);
    fd = open(PIPEFILE, O_RDWR | O_CREAT, 0644);
    write(fd, buff, BUFF_SIZE);

    for(int i = 0; i < 10; i++)
      if(((char**)command2)[i] == NULL){
        ((char**)command2)[i] = (char*)malloc(sizeof(char)*strlen(PIPEFILE));
        strcpy(((char**)command2)[i], PIPEFILE);
        ((char**)command2)[++i] = (char*)malloc(sizeof(char)*2);
        strcpy(((char**)command2)[i], "-a");
        break;
      }
  
    close(fd);
    execvp(((char**)command2)[0], (char**)command2);
  }
  else
    wait(NULL);
  close(client_socket);
  unlink(PIPEFILE);
  FREE_2D_ARRAY(10, (char**)command2);
}

int do_kill(int argc, char** argv)
{
  return kill(atoi(argv[1]), SIGKILL);
}

void err_kill(int err_code)
{
  fprintf(stderr, "kill 실패\n");
}